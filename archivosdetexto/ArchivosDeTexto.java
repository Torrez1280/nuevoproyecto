/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivosdetexto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christian
 */
public class ArchivosDeTexto {
    File archivo;
    File directorio;
    
    private void crearArchivo(){
        archivo = new File("prueba.txt");
        
        try {
            if(archivo.createNewFile()){
                System.out.println("Se creó un nuevo archivo");
            }
        } catch (IOException ex) {
            System.err.println("No se pudo crear el archivo");
        }
    }
    
    private void escribirTexto(){
        try {
            FileWriter escribir = new FileWriter(archivo);
            escribir.write("Hola Mundo");
            escribir.write("\rMás texto");
            escribir.close();
        } catch (IOException ex) {
            System.err.println("Error, no se pudo escribir sobre el archivo");
        }
    }
    
    private void añadirTexto(){
        try {
            FileWriter escribir = new FileWriter(archivo, true);
            escribir.write("\rHola que tal");
            escribir.write("\rretorno de carro");
            escribir.close();
        } catch (IOException ex) {
            System.err.println("Error, no se pudo escribir sobre el archivo");
        }
    }
    
    private void crearDirectorio(){
        directorio = new File("carpetaPrueba");
        
        if(directorio.mkdir()){
            System.out.println("Se creó el directorio");
        }else{
            System.out.println("No se creó el directorio");
        }
    }
    
    public static void main(String[] args) {
        ArchivosDeTexto archivo = new ArchivosDeTexto();
        archivo.crearArchivo();
        archivo.escribirTexto();
        archivo.añadirTexto();
        //archivo.crearDirectorio();
    }
    
}
