/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 *  Iconos creados por: Mark James (mjames at gmail.com) 
 *  http://www.famfamfam.com/lab/icons/silk/
 *  http://www.icongalore.com
 *  Otros iconos gratuitos fueron tomados de: http://www.iconfinder.com
 */
package core;

/**
 *
 * @author William Sánchez
 */
public class SQLServerConnection {
  private String serverName;
  private int portNumber;
  private String instanceName;
  private boolean integratedSecurity;

  private String databaseName;
  private String password;
  private String user;
  
    public SQLServerConnection(){
        super();
    }
    
    public SQLServerConnection(String serverName, int portNumber, String databaseName, String password, String user) {
        this.serverName = serverName;
        this.portNumber = portNumber;
        this.databaseName = databaseName;
        this.password = password;
        this.user = user;
    }
 
    public boolean isIntegratedSecurity() {
        return integratedSecurity;
    }

    public void setIntegratedSecurity(boolean integratedSecurity) {
        this.integratedSecurity = integratedSecurity;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public int getPortNumber() {
        return portNumber;
    }
    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    
    public boolean Connect(){
        //Este código se ha hecho con fines ilustrativos y no es recomendable en lo absoluto incrustar una clave estática 
        // en el código fuente.
        return user.equals("Admin") && password.equals("PII-PROMECYS-IIIC2018");
    }
}
