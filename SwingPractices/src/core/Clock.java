/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 * 
 */
package core;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;

/**
 *
 * @author William Sanchez
 */
public class Clock extends Thread{
    private final JLabel screen;
    SimpleDateFormat hour;
    
    public Clock(JLabel lc){
        screen = lc;
        hour =  new SimpleDateFormat("hh:mm:ss");
        setText(hour.format(new Date()));
    }
      public Clock(){
        screen = new JLabel();
        hour =  new SimpleDateFormat("hh:mm:ss");
        setText(hour.format(new Date()));
    }  
     public void init() {        
        try{
            setText(hour.format(new Date()));
        }
        catch(Exception e){
        }        
    }   
    @Override
    public void run() {
        while(true){
            try{
                setText(hour.format(new Date()));                
                Thread.sleep(1000);
            }
            catch(InterruptedException e){                    
            }
        }
    }
    private void setText(String content) {
        screen.setText(content);
    }
}
